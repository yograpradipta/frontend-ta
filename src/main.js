import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import Router from './routes/'
import Auth from '@/common/auth.js'

Vue.config.productionTip = false
Vue.use(Auth)

new Vue({
  render: h => h(App),
  router: Router
}).$mount('#app')
