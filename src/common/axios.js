import axios from 'axios'

export default axios.create({
 baseURL: "http://127.0.0.1:8000/api"
  // baseURL: "http://34.73.212.55/api"
  // baseURL: "http://transnetsumbar.id/api/api/"
})

// //sebelum requeast jalan
// instance.interceptors.request.use(config =>{
//   NProgress.start()
//   return config
// })

// //sebelum progress stop
// instance.interceptors.response.use(response=>{
//   NProgress.done()
//   return response
// })