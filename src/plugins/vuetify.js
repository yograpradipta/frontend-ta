import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  iconfont: 'md',
  theme: {
    primary: '#0ca613',
    // primary: '#3f51b5',
    secondary: '#424242',
    accent: '#82B1FF',
    error: "#e71a14",
    success: "#2f3bd1",
    warning: '#e9830a',
    danger: 'red',
    other: "#a0d50a",
    admincolor: "#a7f600",
    cardgrey: "#e1f0e5",
    print: "#001686"
  }
})
