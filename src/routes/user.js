
export default {
    path: "/user",
    name: "user",
    meta: {
        forAuth: true
    },
    component: () => import("@/views/user.vue"),
    children: [
        {
            path: "dashboard",
            component: () => import("@/views/user/dashbord.vue")
        },
        {
            path: "alats",
            component: () => import("@/views/user/alats.vue")
        },
        {
            path: "maintenance/:id",
            component: () => import("@/views/user/formMaintenance.vue")
        },
    ]
};
