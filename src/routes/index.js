import Vue from 'vue'
import VueRouter from 'vue-router'
// import hal1 from '@/components/hal1.vue'
import Admin from './admin'
import User from './user'
import login from '@/views/login.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    Admin,
    User,
    {
      path: '/login',
      component: login
    },
    {
      path: "/print-alat",
      component: () => import("@/components/report/printAlat.vue")
    },
    {
      path: "/print-maintenance",
      component: () => import("@/components/report/printMaintenance.vue")
    },
    {
      path: "/print-jenis",
      component: () => import("@/components/report/printJenis.vue")
    },

    {
      path: "/print-keluar",
      component: () => import("@/components/report/printKeluar.vue")
    }
    // {
    //   path: "/hal1",
    //   component: hal1
    // }
  ]
})

router.beforeEach(
  (to, from, next) => {
    if (to.matched.some(record => record.meta.forVisitors)) {
      if (Vue.auth.isAuthenticated()) {
        next({
          path: '/admin/'
        })
      } else next()
    } else if (to.matched.some(record => record.meta.forAuth)) {
      if (!Vue.auth.isAuthenticated()) {
        next({
          path: '/login'
        })
      } else next()
    }
    else next()
  }
)


export default router
