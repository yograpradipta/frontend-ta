import hal1 from "@/components/hal1.vue";

export default {
  path: "/admin",
  name: "admin",
  meta: {
    forAuth: true
  },
  component: () => import("@/views/admin.vue"),
  children: [
    {
      path: "dashboard",
      component: () => import("@/views/dashboard.vue")
    },
    {
      path: "",
      component: () => import("@/views/dashboard.vue")
    },
    // route detail alat
    {
      path: "alat",
      component: () => import("@/views/menu/alats/alat.vue")
    },
    {
      path: "alat/add",
      component: () => import("@/views/menu/alats/input.vue")
    },
    {
      path: "alat/:id/edit",
      component: () => import("@/views/menu/alats/edit.vue")
    },
    {
      path: "alatbykat/:idkat",
      component: () => import("@/views/menu/alats/alatByKat.vue")
    },
    {
      path: "print-alat",
      component: () => import("@/components/report/printAlat.vue")
    },
    {
      path: "alatbykat/:idkat/add",
      component: () => import("@/views/menu/alats/inputByKat.vue")
    },
    {
      path: "alatBykat/:idkat/edit/:id",
      component: () => import("@/views/menu/alats/editByKat.vue")
    },
    // route alat msuk
    {
      path: "alatmasuk",
      component: () => import("@/views/menu/alatMasuks/alatMasuk.vue")
    },
    {
      path: "alatmasuk/add",
      component: () => import("@/views/menu/alatMasuks/input.vue")
    },
    {
      path: "alatmasuk/:id/edit",
      component: () => import("@/views/menu/alatMasuks/edit.vue")
    },
    // route alat keluar
    {
      path: "alatkeluar",
      component: () => import("@/views/menu/alatKeluars/alatKeluar.vue")
    },
    {
      path: "alatkeluar/add",
      component: () => import("@/views/menu/alatKeluars/input.vue")
    },
    {
      path: "alatkeluar/detail/:id",
      component: () => import("@/views/menu/alatKeluars/formKeluar.vue")
    },
    {
      path: "alatkeluar/edit/:id",
      component: () => import("@/views/menu/alatKeluars/formEdit.vue")
    },
    {
      path: "print-keluar",
      component: () => import("@/components/report/printKeluar.vue")
    },
    /* route kalibrasi */
    {
      path: "kalibrasi",
      component: () => import("@/views/menu/dataKalibrasis/kalibrasis.vue")
    },
    {
      path: "kalibrasi/add",
      component: () => import("@/views/menu/dataKalibrasis/input.vue")
    },
    {
      path: "kalibrasi/:id/edit",
      component: () => import("@/views/menu/dataKalibrasis/edit.vue")
    },
    //route kategori alat
    {
      path: "kategorialat/:id/edit",
      component: () => import("@/views/menu/kategoriAlat/edit.vue")
    },
    {
      path: "kategorialat/add",
      component: () => import("@/views/menu/kategoriAlat/input.vue")
    },
    {
      path: "kategorialat",
      component: () => import("@/views/menu/kategoriAlat/kategori.vue")
    },
    //route data bidang
    {
      path: "bidang/:id/edit",
      component: () => import("@/views/menu/bidangs/edit.vue")
    },
    {
      path: "bidang/add",
      component: () => import("@/views/menu/bidangs/input.vue")
    },
    {
      path: "bidang",
      component: () => import("@/views/menu/bidangs/bidang.vue")
    },
    //data maintenance
    {
      path: "maintenance",
      component: () => import("@/views/menu/maintenances/maintenance.vue")
    },
    {
      path: "maintenance/:id/edit",
      component: () => import("@/views/menu/maintenances/formMaintenance.vue")
    },
    {
      path: "print-maintenance",
      component: () => import("@/components/report/printMaintenance.vue")
    },
    {
      path: "print-detail-maintenance/:id",
      component: () => import("@/components/report/printDetailMaintenance.vue")
    },
    //route data instalasi
    {
      path: "instalasi/:id/edit",
      component: () => import("@/views/menu/instalasis/edit.vue")
    },
    {
      path: "instalasi/add",
      component: () => import("@/views/menu/instalasis/input.vue")
    },
    {
      path: "instalasi",
      component: () => import("@/views/menu/instalasis/instalasi.vue")
    },
    //route data ruang
    {
      path: "ruang/:id/edit",
      component: () => import("@/views/menu/ruangs/edit.vue")
    },
    {
      path: "ruang/add",
      component: () => import("@/views/menu/ruangs/input.vue")
    },
    {
      path: "ruang",
      component: () => import("@/views/menu/ruangs/ruang.vue")
    },
    // route data distributor
    {
      path: "distributor/:id/edit",
      component: () => import("@/views/menu/distributors/edit.vue")
    },
    {
      path: "distributor/add",
      component: () => import("@/views/menu/distributors/input.vue")
    },
    {
      path: "distributor",
      component: () => import("@/views/menu/distributors/distributor.vue")
    },
    // route data admin
    {
      path: "admin",
      component: () => import("@/views/menu/dataAdmins/admin.vue")
    },
    {
      path: "admin/add",
      component: () => import("@/views/menu/dataAdmins/input.vue")
    },
    {
      path: "admin/:id/edit",
      component: () => import("@/views/menu/dataAdmins/edit.vue")
    },
    //route data user
    {
      path: "user",
      component: () => import("@/views/menu/user/user.vue")
    },

    {
      path: "user/add",
      component: () => import("@/views/menu/user/input.vue")
    },
    {
      path: "user/:id/edit",
      component: () => import("@/views/menu/user/edit.vue")
    },
    // route data admin
    {
      path: "master-admin",
      component: () => import("@/views/menu/masterAdmin/master.vue")
    },
    {
      path: "master-admin/add",
      component: () => import("@/views/menu/masterAdmin/input.vue")
    },
    {
      path: "master-admin/:id/edit",
      component: () => import("@/views/menu/masterAdmin/edit.vue")
    },
    {
      path: "hal1",
      component: hal1
    }
  ]
};
